/* globals OblakoEditor: false */
// Dirty hacks
(function ($) {

	'use strict';

	function handleEmptyParagraphInQuote(editor, event) {
		var doc = editor.options.ownerDocument,
			node = MediumEditor.selection.getSelectionStart(doc);
		if (node.parentNode.nodeName.toLowerCase() === 'blockquote' && node.childNodes.length === 1 && node.childNodes[0].nodeName.toLowerCase() === 'br') {
			event.preventDefault();
			OblakoEditor.blockUtil.removeFromBlock([node]);
			MediumEditor.selection.moveCursor(doc, node);
			editor.events.triggerCustomEvent('editableInput', event, editor.getFocusedElement());
		}
	}

	function isAllContentSelected(editorElement, range) {
		if (range.commonAncestorContainer === editorElement) {
			var startElement = range.startContainer,
				atBeginning = (range.startOffset === 0),
				endElement = range.endContainer,
				atEnd = false,
				mediumInsertToolbar;

			if (atBeginning) {
				if (endElement.nodeType === Node.TEXT_NODE) {
					atEnd = (range.endOffset === endElement.length);
				} else if (endElement.nodeType === Node.ELEMENT_NODE) {
					atEnd = (range.endOffset === endElement.childNodes.length);
					if (!atEnd) {
						var trailingBrs = true,
							i;
						for (i = range.endOffset; i < endElement.childNodes.length; i++) {
							if (endElement.childNodes[i].nodeType !== Node.ELEMENT_NODE ||
									endElement.childNodes[i].tagName.toLowerCase() !== 'br') {
								trailingBrs = false;
								break;
							}
						}
						atEnd = trailingBrs;
					}
				}
				if (atEnd) {
					mediumInsertToolbar = editorElement.lastElementChild;
					if (!mediumInsertToolbar.classList.contains('medium-insert-buttons')) {
						mediumInsertToolbar = null;
					}

					while (atBeginning && (startElement !== editorElement)) {
						if (startElement.previousElementSibling === null) {
							startElement = startElement.parentNode;
						} else {
							atBeginning = false;
						}
					}
					if (atBeginning) {
						while (atEnd && (endElement !== editorElement)) {
							var nextElement = endElement.nextElementSibling;
							if (nextElement === null || nextElement === mediumInsertToolbar) {
								endElement = endElement.parentNode;
							} else {
								while (nextElement !== null && nextElement.nodeType === Node.ELEMENT_NODE && nextElement.tagName.toLowerCase() === 'br') {
									nextElement = nextElement.nextElementSibling;
								}
								if (nextElement === null || nextElement === mediumInsertToolbar) {
									endElement = endElement.parentNode;
								} else {
									atEnd = false;
								}
							}
						}
					}
					if (atBeginning && atEnd) {
						return true;
					}
				}
			}
		}
		return false;
	}

	var Fixes = MediumEditor.Extension.extend({
		name: 'custom-fixes',
		tableCellTags: ['th', 'td'],

		setup: function () {
			// this.disableKeyboardCombinations();
			this.handleDeletePress();
			this.handleEnterPress();
			this.handleContentReplacement();
			this.handleCtrlA();
			this.disableAnchorPreview();
			this.enableTableFix();
			// this.handleWildBR();
			this.mediumInsertFixes();
		},

		// Disable (ctrl|meta)+z and shift+enter
		disableKeyboardCombinations: function () {
			this.getEditorElements().forEach(function (element) {
				this.on(element, 'keydown', function (event) {
					if ((!!event.metaKey || !!event.ctrlKey) && event.keyCode === 90) {
						event.preventDefault();
					}
					if (!!event.shiftKey && event.keyCode === 13) {
						event.preventDefault();
					}
				});
			}, this);
		},

		handleDeletePress: function () {
			// Disable Medium's logic on pressing delete
			var defaultDeleteAction = this.base.events.customEvents['editableKeydownDelete'][0];
			this.base.unsubscribe('editableKeydownDelete', defaultDeleteAction);

			// Let escape from blockquote by pressing Delete while in empty line
			this.base.subscribe('editableKeydownDelete', function (event) {
				handleEmptyParagraphInQuote(this.base, event);
			}.bind(this));

			this.base.subscribe('editableKeydownDelete', function (event, editorElement) {

				var selection = this.document.getSelection(),
					range = selection.getRangeAt(0),
					miButtons = this.document.getElementsByClassName('medium-insert-buttons')[0],
					elem = range.startContainer,
					target;

				// When everything is selected, don't delete Medium Insert buttons
				if (range.intersectsNode(miButtons)) {
					event.preventDefault();
					while (editorElement.firstChild !== null && editorElement.firstChild !== miButtons) {
						editorElement.removeChild(editorElement.firstChild);
					}
					var p = this.document.createElement('p');
					p.className = 'medium-insert-active';
					p.innerHTML = '<br>';
					p = editorElement.insertBefore(p, editorElement.firstChild);
					MediumEditor.selection.moveCursor(this.document, p);
					this.base.trigger('editableInput', null, editorElement);
					return;
				}

				// if we have several table cells within our selection, do nothing
				if (!range.collapsed) {
					var selected = MediumEditor.selection.getSelectedElements(this.document),
						tableSelected = false,
						selectedCells = 0,
						i;
					for (i = 0; i < selected.length; i++) {
						if (selected[i].nodeName.toLowerCase() === 'table') {
							tableSelected = true;
							if (!selection.containsNode(selected[i].previousElementSibling.lastChild, true) ||
								!selection.containsNode(selected[i].nextElementSibling.firstChild, true)) {
								event.preventDefault();
								return;
							}
						}
					}
					if (!tableSelected) {
						for (i = 0; i < selected.length; i++) {
							if (this.tableCellTags.indexOf(selected[i].nodeName.toLowerCase()) >= 0) {
								selectedCells++;
								if (selectedCells >= 3) {
									event.preventDefault();
									return;
								}
							}
						}
						if (selectedCells === 2) {
							if (range.endOffset === 0) {
								var pos;
								if (selected.nodeType === Node.TEXT_NODE) {
									pos = selected.Text().length;
								} else {
									pos = selected.childNodes.length;
								}
								range.setEnd(selected, pos);
								return;
							} else {
								event.preventDefault();
							}
							return;
						}
					}

					// when everything is selected, replace content to prevent Firefox from breaking its own undo stack
					if (isAllContentSelected(editorElement, range)) {
						document.execCommand('insertHTML', false, '<p class="medium-insert-active"> </p>');
						event.preventDefault();
						return;
					}
				}

				if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.BACKSPACE)) {
					if (range.collapsed) {
						if (range.startOffset === 1) {
							// If we delete second of two empty paragraphs,
							// then selection goes *after* <br> in first paragraph
							// (at least) in Firefox and Chrome
							if (elem.firstElementChild && elem.firstElementChild.nodeName.toLowerCase() === 'br' &&
								elem.previousElementSibling === null) {
								event.preventDefault();
								return;
							}
						} else if (range.startOffset === 0) {
							while (elem.parentNode !== editorElement) {
								elem = elem.parentNode;
							}
							if (elem.nodeType === Node.ELEMENT_NODE && elem.nodeName.toLowerCase() === 'table') {
								return;
							}

							target = elem.previousElementSibling;

							// Disable default if we are in the beginning of text
							if (target === null && elem.textContent.length === 0) {
								event.preventDefault();
								return;
							}

							// Fix: prevent deleting tables if deleting image, which is the first child
							if (target && target.classList.contains('medium-insert-images')) {
								target.parentNode.removeChild(target);
								event.preventDefault();
								return;
							}
						}
					}
				} else {
					// Delete pressed
					if (range.collapsed) {
						while (elem.parentNode !== editorElement) {
							elem = elem.parentNode;
						}
						if (elem.nodeType === Node.ELEMENT_NODE && elem.nodeName.toLowerCase() === 'table') {
							return;
						}
						target = elem.nextElementSibling;
						if (target !== null && target.classList.contains('medium-insert-buttons')) {
							event.preventDefault();
							return;
						}
					}
				}
				if (target !== undefined && target !== null) {
					var prevent = false;
					// Handling deleting placeholder-blocks
					if (target.classList.contains('placeholder-block')) {
						target.parentNode.removeChild(target);
						this.base.trigger('editableInput', null, editorElement);
						prevent = true;
					} else if (target.nodeType === Node.ELEMENT_NODE && target.nodeName.toLowerCase() === 'table') {
						// Don't delete tables
						prevent = true;
					}
					// } else {
					// 	// Handling deleting paragraphs (made for Firefox)
					// 	elem.parentNode.removeChild(elem);
					// 	MediumEditor.selection.moveCursor(this.document, target, target.childNodes.length);
					// }
					if (prevent) {
						event.preventDefault();
						return;
					}
				}
			}.bind(this));
		},

		// Let escape from blockquote by pressing Enter while in empty line
		handleEnterPress: function () {
			this.base.subscribe('editableKeydownEnter', function (event) {
				handleEmptyParagraphInQuote(this.base, event);
			}.bind(this));
		},

		handleContentReplacement: function() {
			this.base.subscribe('editableKeypress', function (event, editorElement) {
				var selection = this.document.getSelection(),
					range = selection.getRangeAt(0);
				if (!event.metaKey && !event.altKey && !event.ctrlKey && !range.collapsed) {
					if (isAllContentSelected(editorElement, range)) {
						var str = '<p class="medium-insert-active"> </p>';
						document.execCommand('insertHTML', false, str);
						var p = editorElement.querySelector('.medium-insert-active');
						MediumEditor.selection.moveCursor(this.document, p, p.childNodes.length);
						return;
					}
				}
			}.bind(this));
		},

		// Select everything but last element (Medium Insert buttons)
		handleCtrlA: function () {
			this.getEditorElements().forEach(function (element) {
				this.on(element, 'keydown', function (event) {
					if ((!!event.metaKey || !!event.ctrlKey) && event.keyCode === 65) {
						event.preventDefault();

						var selection = this.document.getSelection(),
							range = this.document.createRange(),
							ending = null;

						if (element.lastElementChild !== null) {
							selection.removeAllRanges();
							if (element.lastElementChild.classList.contains('medium-insert-buttons')) {
								ending = element.lastElementChild.previousElementSibling;
							} else {
								ending = element.lastElementChild;
							}
							if (ending !== null) {
								range.setStart(element.firstElementChild, 0);
								range.setEnd(ending, ending.childNodes.length);
								selection.addRange(range);
							}
						}
					}
				}.bind(this));
			}, this);
		},

		// Disable anchor preview for table builder cells
		disableAnchorPreview: function () {
			var elements = this.document.getElementsByClassName('medium-editor-table-builder-cell');
			for (var i = 0; i < elements.length; i++) {
				elements[i].dataset.disablePreview = true;
			}
		},

		// Adding empty paragraphs before and after table if needed
		enableTableFix: function () {
			var tableExtension = this.base.getExtensionByName('table');
			if (tableExtension !== undefined) {
				tableExtension.builder.grid._callback = function (rows, columns) {
					if (rows > 0 || columns > 0) {
						tableExtension.table.insert(rows, columns);
						var tableNode = MediumEditor.selection.getSelectionStart(this.document),
							p, sibling;
						while (tableNode.nodeType !== Node.ELEMENT_NODE || tableNode.nodeName.toLowerCase() !== 'table') {
							if (tableNode.parentNode !== null) {
								tableNode = tableNode.parentNode;
							} else {
								return; // wat
							}
						}
						sibling = tableNode.previousElementSibling;
						if (sibling === null ||
								(sibling.nodeType === Node.ELEMENT_NODE && sibling.nodeName.toLowerCase() === 'table')) {
							p = this.document.createElement('p');
							p.innerHTML = '<br>';
							p = tableNode.parentNode.insertBefore(p, tableNode);
						}
						sibling = tableNode.nextElementSibling;
						if (sibling === null || sibling.classList.contains('medium-insert-buttons') ||
								(sibling.nodeType === Node.ELEMENT_NODE && sibling.nodeName.toLowerCase() === 'table')) {
							p = this.document.createElement('p');
							p.innerHTML = '<br>';
							p = tableNode.parentNode.insertBefore(p, tableNode.nextSibling);
						}
					}
					tableExtension.hide();
					OblakoEditor.util.hideMediumInsert(this.base);
				}.bind(this);
			}
		},

		// Replace br with p, if it is the first child of editor
		// Breaks undo stack
		// TODO: discard or replace using document.execCommand
		handleWildBR: function () {
			this.base.subscribe('editableInput', function (event, editorElement) {
				var element = editorElement.firstElementChild,
					mediumInsertCount = editorElement.getElementsByClassName('medium-insert-buttons').length, // 0 or 1
					p, br;

				if (editorElement.children.length - mediumInsertCount === 1) {
					if (element.nodeName.toLowerCase() === 'br') {
						p = this.document.createElement('p');
						br = this.document.createElement('br');
						editorElement.replaceChild(p, element);
						p.appendChild(br);
						element = p;
						MediumEditor.selection.moveCursor(this.document, p);
					}
				}
			}.bind(this));
		},

		mediumInsertFixes: function () {
			if (OblakoEditor.mediumInsertEnabled) {
				// Hiding MediumInsert on editor blur
				this.base.subscribe('blur', function (event, editorElement) {
					OblakoEditor.util.hideMediumInsert(this.base, editorElement);
				}.bind(this));

				// Hide table builder before MediumInsert's buttons show
				$('.medium-insert-buttons-show').click(function () {
					if ($('.medium-insert-buttons-addons').css('display') === 'none') {
						var table = this.base.getExtensionByName('table');
						if (table !== undefined) {
							table.hide();
						}
					}
				}.bind(this));

				$('.medium-insert-buttons').addClass('unselectable');
				$('.medium-insert-buttons-show').addClass('unselectable');

				// Hide MediumInsert Addons on click on empty line
				this.base.subscribe('editableClick', function (event, editorElement) {
					var mediumInsert = $(editorElement).data('plugin_mediumInsert'),
						miButtons = this.document.getElementsByClassName('medium-insert-buttons')[0],
						miButtonsAddons = this.document.getElementsByClassName('medium-insert-buttons-addons')[0];
					if (mediumInsert !== undefined && miButtonsAddons.style.display !== 'none' && !miButtons.contains(event.target)) {
						mediumInsert.hideAddons();
					}
				}.bind(this));
			}
		}

	});

	OblakoEditor.extensions.fixes = Fixes;

})(jQuery);
