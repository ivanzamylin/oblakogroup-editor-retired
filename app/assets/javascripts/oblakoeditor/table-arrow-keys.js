/* globals OblakoEditor: false */
(function () {

	'use strict';

	var ArrowKeys = MediumEditor.Extension.extend({
		name: 'table-arrow-keys',

		init: function () {
			this.subscribe('editableKeydown', this.handleKeydown.bind(this));
		},

		handleKeydown: function (event, editorElement) {
			var range = MediumEditor.selection.getSelectionRange(this.document),
				element = MediumEditor.selection.getSelectedParentElement(range);
			if (element === editorElement) {
				return;
			}
			if (editorElement.contains(element)) {
				while (element !== editorElement) {
					if (element.nodeType === Node.ELEMENT_NODE && element.tagName.toLowerCase() === 'td') {
						this.checkKey(event, element, range);
						return;
					}
					element = element.parentNode;
				}
			}
		},

		checkKey: function (event, element) {
			if (event.keyCode === 38) {
				this.moveCursor(event, element, false);
			} else if (event.keyCode === 40) {
				this.moveCursor(event, element, true);
			}
		},

		moveCursor: function (event, element, down) {
			var index = this.getIndexAmongSiblings(element),
				targetRow;

			if (down === true) {
				targetRow = element.parentNode.nextSibling;
			} else {
				targetRow = element.parentNode.previousSibling;
			}
			if (targetRow !== null) {
				event.preventDefault();
				MediumEditor.selection.moveCursor(this.document, targetRow.children[index]);
			}
		},

		getIndexAmongSiblings: function (element) {
			var parent = element.parentNode,
				index = Array.prototype.indexOf.call(parent.children, element);
			return index;
		}
	});

	OblakoEditor.extensions.tableArrowKeys = ArrowKeys;

})();