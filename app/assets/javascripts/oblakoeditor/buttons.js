/* globals OblakoEditor: false */
(function () {

	'use strict';

	// TODO: Change *var* to *let* someday
	// 'Abstract' class
	var BlockHandler = MediumEditor.extensions.button.extend({
		blocktags: ['blockquote', 'h3', 'ul', 'ol'],
		listtag: 'li',
		tag: '',

		init: function () {
			MediumEditor.extensions.button.prototype.init.call(this);
		},

		/*	document = this.document,
			editor = this.base,
			window = this.window,
			util = MediumEditor.util,
			selection = MediumEditor.selection */

		handleClick: function (event) {
			event.preventDefault();
			event.stopPropagation();

			var nodes = OblakoEditor.blockUtil.getSelectedNodes(this.document, this.base.getFocusedElement()),
				blocks = [],
				addScenario = false,
				removeScenario = true;

			for (var i = 0; i < nodes.length; i++) {
				blocks.push(OblakoEditor.blockUtil.getBlockName(nodes[i], this.base.getFocusedElement()));
			}

			if (blocks.length === 0) {
				return;
			}

			for (var j = 0; j < blocks.length; j++) {
				if (blocks[j] !== this.tag) {
					addScenario = true;
					removeScenario = false;
				}
			}

			if (addScenario) {
				this.appendToBlock(nodes);
			} else if (removeScenario) {
				OblakoEditor.blockUtil.removeFromBlock(nodes);
			}

			this.base.getExtensionByName('toolbar').hideToolbar();
			this.base.events.triggerCustomEvent('editableInput', event, this.base.getFocusedElement());
		},

		appendToBlock: function (nodes) {
			OblakoEditor.blockUtil.appendToBlock(nodes, this.tag);
		}
	});

	OblakoEditor.extensions.blockHandler = BlockHandler;

	var Header = BlockHandler.extend({
		name: 'custom-header',
		tagNames: ['h3'],
		contentDefault: '<b>H</b>',
		contentFA: '<i class="fa fa-header"></i>',
		aria: 'header',

		tag: 'h3',

		init: function () {
			BlockHandler.prototype.init.call(this);
		}
	});

	OblakoEditor.extensions.header = Header;

	var Quote = BlockHandler.extend({
		name: 'custom-quote',
		tagNames: ['blockquote'],
		contentDefault: '<b>&ldquo;</b>',
		contentFA: '<i class="fa fa-quote-right"></i>',
		aria: 'blockquote',

		tag: 'blockquote',

		init: function () {
			BlockHandler.prototype.init.call(this);
		}
	});

	OblakoEditor.extensions.quote = Quote;

	var UnorderedList = BlockHandler.extend({
		name: 'custom-ul',
		tagNames: ['ul'],
		contentDefault: '<b>&bull;</b>',
		contentFA: '<i class="fa fa-list-ul"></i>',
		aria: 'unordered list',

		tag: 'ul',
		listItemTag: 'li',

		init: function () {
			BlockHandler.prototype.init.call(this);
		},

		appendToBlock: function (nodes) {
			OblakoEditor.blockUtil.appendToListBlock(nodes, this.tag);
		}
	});

	OblakoEditor.extensions.unorderedList = UnorderedList;

	var OrderedList = BlockHandler.extend({
		name: 'custom-ol',
		tagNames: ['ol'],
		contentDefault: '<b>1.</b>',
		contentFA: '<i class="fa fa-list-ol"></i>',
		aria: 'ordered list',

		tag: 'ol',
		listItemTag: 'li',

		init: function () {
			BlockHandler.prototype.init.call(this);
		},

		appendToBlock: function (nodes) {
			OblakoEditor.blockUtil.appendToListBlock(nodes, this.tag);
		}
	});

	OblakoEditor.extensions.orderedList = OrderedList;

})();
