(function ($, window, document, undefined) {

	'use strict';

	/** Default values */
	var pluginName = 'mediumInsert',
		addonName = 'RazdatkaNote', // first char is uppercase
		defaults = {
			label: '<span class="fa fa-arrows-h medium-insert-draw-line"></span>'
		};

	/**
	 * RazdatkaNote Addon object
	 *
	 * Sets options, variables and calls init() function
	 *
	 * @constructor
	 * @param {DOM} el - DOM element to init the plugin on
	 * @param {object} options - Options to override defaults
	 * @return {void}
	 */

	function RazdatkaNote(el, options) {
		this.el = el;
		this.$el = $(el);
		this.templates = window.MediumInsert.Templates;
		this.core = this.$el.data('plugin_' + pluginName);

		this.options = $.extend(true, {}, defaults, options);

		this._defaults = defaults;
		this._name = pluginName;

		this.init();
	}

	/**
	 * Initialization
	 *
	 * @return {void}
	 */

	RazdatkaNote.prototype.init = function () {
		this.events();
	};

	/**
	 * Event listeners
	 *
	 * @return {void}
	 */

	RazdatkaNote.prototype.events = function () {

	};

	/**
	 * Get the Core object
	 *
	 * @return {object} Core object
	 */
	RazdatkaNote.prototype.getCore = function () {
		return this.core;
	};

	/**
	 * Add custom content
	 *
	 * This function is called when user click on the addon's icon
	 *
	 * @return {void}
	 */

	// RazdatkaNote.prototype.add = function () {
	// 	// Finding current place
	// 	var doc = this.core.options.editor.options.ownerDocument,
	// 		place = this.$el.find('.medium-insert-active'),
	// 	// Creating content to insert
	// 		id = 'note_' + ($('.placeholder-block').size() + 1).toString(),
	// 		placeholderBlock = $('<p class="placeholder-block medium-insert-active" contenteditable="false" id="' + id + '"></p>'),
	// 		afterLine = $(('<p class="medium-insert-active"><br/></p>'));

	// 	// Inserting content
	// 	place.replaceWith(placeholderBlock);
	// 	place = this.$el.find('.placeholder-block.medium-insert-active');
	// 	afterLine.insertAfter(place);

	// 	MediumEditor.selection.moveCursor(doc, afterLine[0]);

	// 	// Cleanup
	// 	place.click();
	// 	this.core.hideButtons();
	// };

	RazdatkaNote.prototype.add = function () {
		// Finding current place
		var doc = this.core.options.editor.options.ownerDocument,
			place = this.$el.find('.medium-insert-active'),
		// Creating content to insert
			id = 'note_' + ($('.placeholder-block').size() + 1).toString(),
			placeholderBlockHTML = '<p class="placeholder-block medium-insert-active" contenteditable="false" id="' + id + '"></p>',
			placeholderBlock;

		document.execCommand('insertHTML', false, placeholderBlockHTML);
		placeholderBlock = document.getElementById(id);
		MediumEditor.selection.moveCursor(doc, placeholderBlock.nextElementSibling);
		placeholderBlock.setAttribute('contenteditable', false)

		// Cleanup
		place.click();
		this.core.hideButtons();
	};

	/** Addon initialization */

	$.fn[pluginName + addonName] = function (options) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName + addonName)) {
				$.data(this, 'plugin_' + pluginName + addonName, new RazdatkaNote(this, options));
			}
		});
	};

})(jQuery, window, document);