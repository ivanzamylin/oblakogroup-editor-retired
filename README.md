# OblakoEditor

## Установка 

Разместите в `gemfile`

```
#!ruby
gem 'oblakoeditor', git: 'git@bitbucket.org:ivanzamylin/oblakogroup-editor.git'
```
Выполните `bundle install`

Документацию по использованию гемов из git репозитория можно найти [здесь](http://bundler.io/git.html)

Добавьте в `application.js`

```
//= require oblakoeditor
```
Добавьте в `application.css`
```
 *= require oblakoeditor
```

## Настройка

Варианты настройки.

1. Указать настройки в файле `config/oblakoeditor.yml`
   Настройки будут применены автоматически при старте Rails.
   В случае отсутствия файла будут использованы настройки по умолчанию

2. Использовать класс `Oblakoeditor::Settings` вручную

```
#!ruby
settings = Oblakoeditor::Settings.new
settings.load_yaml 'path_to_settings.yml'
```

[**Список настроек**](SETTINGS.md)
## Использование

Разместите в виде в нужном месте *view helper* `oblakoeditor`

Настройки по умолчанию / загруженные из `config/oblakoeditor.yml`

* `content` - содержимое редактора при иницаиализации
* `id` - значение атрибута id, присваемое элементу редактора

```
#!ruby
<%= oblakoeditor content, id %>
```
Использование класса `Oblakoeditor::Settings`

* `settings` - объект класса `Oblakoeditor::Settings`

```
#!ruby
<%= oblakoeditor content, id, settings %>
```

## Взаимодействие с редактором через Javascript

* объект редактора `OblakoEditor.editor`
* получить содержимое редактора `OblakoEditor.getEditorContent()`
* удалить объект редактора `OblakoEditor.destroyEditor()`

## Используемые Javascript библиотеки

* [MediumEditor](https://github.com/yabwe/medium-editor)

* [MediumEditor Tables](https://github.com/yabwe/medium-editor-tables)

* [MediumEditor Insert Plugin](https://github.com/orthes/medium-editor-insert-plugin)

  * jQuery
 
  * [Handlebars](http://handlebarsjs.com/)

  * [jQuery Sortable](https://github.com/johnny/jquery-sortable)

  * [jQuery File Upload Plugin](https://github.com/blueimp/jQuery-File-Upload)
    * jQuery UI widget factory (включено в jQuery File Upload Plugin)

    * jQuery Iframe Transport plugin (включено в jQuery File Upload Plugin)

## Примечания

### 1

MediumEditor не видит событий, которые произошли вне `body`, поэтому нужно, чтобы `body` занимал всю видимую высоту.

Например:
```
#!scss
html {
  height: 100%;
}
body {
  min-height: 100%;
}
```

### 2

Пример задания стиля `blockquote`.

```
#!scss
.oblakoeditor {
    blockquote {
        border-left: 2px solid grey;
        margin-left: 20px;
        padding-left: 20px;
    }
}
```