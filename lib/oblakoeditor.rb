require 'oblakoeditor/engine'
require 'oblakoeditor/version'
require "oblakoeditor/settings"

module Oblakoeditor
	mattr_accessor :time
	@@time = Time.now

	mattr_accessor :custom_initializer, :custom_initializer_asset
	@@custom_initializer = false
end
