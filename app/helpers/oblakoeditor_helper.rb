module OblakoeditorHelper
	def oblakoeditor content, editor_id = "oblakoeditor", settings = Oblakoeditor.settings
		render partial: "oblakoeditor/editor", locals: { content: content, settings: settings, editor_id: editor_id }
	end

	def write_value name, var
		return raw("#{name}: #{ var.to_json.html_safe },") unless var.nil?
	end
end