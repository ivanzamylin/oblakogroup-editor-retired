/* globals OblakoEditor: false */
(function ($) {

	'use strict';

	var Util = {
		hideMediumInsert: function (editor, editorElement) {
			if (editorElement === undefined) {
				editorElement = editor.getFocusedElement();
			}
			if (editor._hideInsertButtons !== undefined) {
				editor._hideInsertButtons($(editorElement));
			}
		}
	};

	OblakoEditor.util = Util;

})(jQuery);