# Список настроек

Ниже приведен список настроек, разделенных на секции.

Для задания значения в YAML файле нужно указать название секции (**с маленькой буквы, в camelCase**), а в нем название параметра.

Пример

```
#!yaml
anchorPreview:
  enabled: false
toolbar:
  buttons: [‘bold’, ‘italic’]
```

Как правило, настройки соотвествуют соответствующим настройкам MediumEditor.

Прочерк в значении по умолчанию указывает, на то, что параметр не передается в MediumEditor, что значит, что используется *его* значение по умолчанию.

Прочитать о настройках MediumEditor можно [здесь](https://github.com/yabwe/medium-editor/blob/master/OPTIONS.md)

# Core

Значение в YAML: *core*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| activeButtonClass            | -                                          |
| allowMultiParagraphSelection | -                                          |
| buttonLabels                 | -                                          |
| contentWindow                | -                                          |
| delay                        | -                                          |
| disableReturn                | -                                          |
| disableReturn                | -                                          |
| disableExtraSpaces           | -                                          |
| disableEditing               | -                                          |
| elementsContainer            | -                                          |
| ownerDocument                | -                                          |
| spellcheck                   | -                                          |
| targetBlank                  | -                                          |

# Toolbar

Значение в YAML: *toolbar*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| buttons                      | ['bold', 'italic', 'anchor', 'custom-header', 'custom-quote', 'custom-ul', 'custom-ol', '{ name: 'justifyLeft', style: { prop: 'text-align', value: 'left][start' } }', 'justifyCenter']                |
| allowMultiParagraphSelection | -                                          |
| diffLeft                     | -                                          |
| diffTop                      | -                                          |
| standardizeSelectionStart    | -                                          |
| firstButtonClass             | -                                          |
| lastButtonClass              | -                                          |
| static                       | -                                          |
| align                        | -                                          |
| sticky                       | -                                          |
| updateOnEmptySelection       | -                                          |

*custom* в названии кнопок указывает на то, что используется плагин из oblakoeditor/buttons.js, а не встроенный в MediumEditor.

Вместо ][ должен стоять знак вертикальной черты `|` _(Bitbucket пока что его не поддерживает в таблице)_

# Anchor Preview

Значение в YAML: *anchorPreview*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| hideDelay                    | -                                          |
| previewValueSelector         | -                                          |
| showWhenToolbarIsVisible     | -                                          |

# Placeholder

Значение в YAML: *placeholder*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| text                         | 'Введите текст'                            |
| hideOnClick                  | -                                          |

# Anchor Form

Значение в YAML: *anchorForm*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| linkValidation               | true                                       |
| placeholderText              | 'Введите адрес ссылки'                     |
| customClassOption            | -                                          |
| customClassOptionText        | -                                          |
| targetCheckbox               | -                                          |
| targetCheckboxText           | -                                          |

# Paste

Значение в YAML: *paste*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| forcePlainText               | false                                      |
| cleanPastedHTML              | true                                       |
| cleanReplacements            | -                                          |
| cleanAttrs                   | ['dir', 'id', 'class', 'style', 'offline', 'onabort', 'onafterprint', 'onbeforeonload', 'onbeforeprint', 'onblur', 'oncanplay', 'oncanplaythrough', 'onchange', 'onclick', 'oncontextmenu', 'ondblclick', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'ondurationchange', 'onemptied', 'onended', 'onerror', 'onfocus', 'onformchange', 'onforminput', 'onhaschange', 'oninput', 'oninvalid', 'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onloadeddata', 'onloadedmetadata', 'onloadstart', 'onmessage', 'onmousedown', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onoffline', 'onoine', 'ononline', 'onpagehide', 'onpageshow', 'onpause', 'onplay', 'onplaying', 'onpopstate', 'onprogress', 'onratechange', 'onreadystatechange', 'onredo', 'onresize', 'onscroll', 'onseeked', 'onseeking', 'onselect', 'onstalled', 'onstorage', 'onsubmit', 'onsuspend', 'ontimeupdate', 'onundo', 'onunload', 'onvolumechange', 'onwaiting']                |
| cleanTags                    | ['iframe', 'meta', 'script', 'img']        |

# KeyboardCommands

Значение в YAML: *keyboardCommands*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| commands                     | -                                          |

# AutoLink

Значение в YAML: *autoLink*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |


# ImageDragging

Значение в YAML: *imageDragging*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| useInsertPlugin              | true                                       |

Если useInsertPlugin = true, то используется плагин OblakoEditor.extensions.imageDragging, который использует загрузку изображения из MediumEditor Insert Plugin, а не плагин, встроенный в MediumEditor

# Fixes

Значение в YAML: *fixes*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |

# Tables

Настройки MediumEditor Tables

Значение в YAML: *tables*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| useInsertPlugin              | true                                       |
| useEditor                    | true                                       |
| arrowKeys                    | true                                       |
| rows                         | -                                          |
| columns                      | -                                          |
| commands                     | -                                          |

Если enabled = true, то значение 'tables' автоматически добавлено в toolbar.buttons, если там его нет.

Если useInsertPlugin = true, то кнопка плагина будет перемещена в список кнопок плагина MediumEditor Insert Plugin (если он включен).

Если useEditor = true, то будет использован плагин OblakoEditor.extensions.tableEditor для редактирования таблиц.

Если arrowKeys = true, то мы можем перемещаться вверх/вниз по таблице с помощью соответствующих кнопок.

# Insert

Настройки MediumEditor Insert Plugin

Список настроек можно найти [здесь](https://github.com/orthes/medium-editor-insert-plugin/wiki/v2.x-Configuration)

Значение в YAML: *insert*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |

## Insert: Images

Настройки плагина загрузки изображений из MediumEditor Insert Plugin

Значение в YAML: *insert[images]*

То есть, чтобы выключить плагин, нужно указать
```
#!yaml
insert:
  images:
    enabled: false
```

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | true                                       |
| captionPlaceholder           | 'Введите подпись к картинке (опционально)' |
| label                        | -                                          |
| uploadScript                 | -                                          |
| deleteScript                 | -                                          |
| deleteMethod                 | -                                          |
| preview                      | -                                          |
| captions                     | -                                          |
| autoGrid                     | -                                          |
| formDatad                    | -                                          |
| fileUploadOptions            | -                                          |
| fileDeleteOptions            | -                                          |
| messages                     | -                                          |
| uploadCompleted              | -                                          |

## Insert: Embeds

Настройки плагина embeds из MediumEditor Insert Plugin

Значение в YAML: *insert[embeds]*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | false                                      |
| captionPlaceholder           | 'Введите подпись к картинке (опционально)' |
| deleteScript                 | -                                          |
| deleteMethod                 | -                                          |
| preview                      | -                                          |
| captions                     | -                                          |
| autoGrid                     | -                                          |
| fileUploadOptions            | -                                          |
| fileDeleteOptions            | -                                          |
| messages                     | -                                          |
| uploadCompleted              | -                                          |


## Insert: RazdatkaNote

Настройки плагина RazdatkaNote

Значение в YAML: *insert[razdatkaNote]*

| Параметр                     | Значение по умолчанию                      |
| ---------------------------- | -------------------------------------------|
| enabled                      | false                                       |